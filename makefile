.DEFAULT_GOAL:= run
.PHONY: doc run build lint clean

LOVE = love
SOURCE_DIR = src
DOC_DIR = doc
BUILD_DIR = build

doc:
	ldoc $(SOURCE_DIR)/Automaton -s $(DOC_DIR) --dir $(DOC_DIR)

run:
	$(LOVE) $(SOURCE_DIR) --fused

build:
	love-release -W 64 $(BUILD_DIR) $(SOURCE_DIR) --title Automaton

lint:
	luacheck --std love+luajit --ignore 14[23] -- $(SOURCE_DIR)

clean:
	rm -r $(DOC_DIR)/* ||:
	rm -r $(BUILD_DIR)/* ||:
