love.run = require "run"

love.graphics.setFont(love.graphics.newFont(25))

local Tile      = require "Automaton.Tile"
local ModLoader = require "Automaton.ModLoader"
local Player    = require "Automaton.Player"
local Camera    = require "Automaton.Camera"
local Chunk     = require "Automaton.Chunk"
local TileData  = require "Automaton.TileData"
local Light     = require "Automaton.Light"

local red_light = Light(0, 0, 500, 1, 0, 0)
local green_light = Light(0, 0, 500, 0, 1, 0)
local blue_light = Light(0, 0, 500, 0, 0, 1)

ModLoader.preload_default()
ModLoader.load()

local width, height = love.graphics.getDimensions()
width, height = width / 2, height / 2

local tpi = (math.pi * 2) / 3

function love.update(dt)
	Player:update(dt)
	Camera.update(dt)

	local d = 400 * math.sin(love.timer.getTime())

	red_light:set_position(math.sin(love.timer.getTime()) * d + width, math.cos(love.timer.getTime()) * d + height)
	green_light:set_position(math.sin(love.timer.getTime() + tpi) * d + width, math.cos(love.timer.getTime() + tpi) * d + height)
	blue_light:set_position(math.sin(love.timer.getTime() - tpi) * d + width, math.cos(love.timer.getTime() - tpi) * d + height)
end

function love.draw()
	Chunk.draw_all()
end

local stats
function love.ui()
	stats = love.graphics.getStats()

	love.graphics.print(string.format(
[[Drawcalls: %i
Texture memory: %i KB
Average frame delta: %i ms
Chunk buffer size: %i
]], stats.drawcalls, stats.texturememory / 1000, love.timer.getAverageDelta() * 1000, #Chunk.chunk_list))

	love.graphics.print(tostring(Camera.x) .. ", " .. tostring(Camera.y), 0, 300)

	love.graphics.draw(Tile.normal)
end

local mode = false

function love.keypressed(key)
	if key == "c" then
		Chunk.collect_garbage()
	elseif key == "escape" then
		love.event.quit()
	elseif key == "space" then
		mode = not mode
		Chunk.shader:send("mode", mode)
	end
end

local test_tile = TileData(5, 1, 0, 0.5)

function love.mousepressed(x, y, button)
	if button == 1 then
		local world_x, world_y = Camera.screen_to_world(x, y)
		Chunk.set_tile(world_x, world_y, test_tile)
	end
end