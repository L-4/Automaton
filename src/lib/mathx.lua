local mathx = {}

local floor = math.floor
local sqrt = math.sqrt

function mathx.round(val)
	return floor(val + 0.5)
end

function mathx.distance(x1, y1, x2, y2)
	x1, y1 = x1 - x2, y1 - y2
	return sqrt(x1*x1+y1*y1)
end

function mathx.point_in_aabb(x, y, box_x, box_y, box_w, box_h)
	return x > box_x and
				 y > box_y and
				 x < box_x + box_w and
				 y < box_y + box_h
end

function mathx.aabb(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2 + w2 and
         x2 < x1 + w1 and
         y1 < y2 + h2 and
         y2 < y1 + h1
end

function mathx.lerp(a, b, delta)
	return a + ((b - a) * delta)
end

function mathx.lerp_vec(x1, y1, x2, y2, delta)
	return mathx.lerp(x1, x2, delta), mathx.lerp(y1, y2, delta)
end

return mathx