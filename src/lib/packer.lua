local packer = require "lib.class"() -- @todo: Optimization: http://blackpawn.com/texts/lightmaps/

function packer:__init(w, h)
	self.w, self.h = w, h
	self.highest_x, self.highest_y = 0, 0

	return self
end

local function block_sorter(block_a, block_b)
	return block_a.h < block_b.h
end

function packer:pack(blocks)
	table.sort(blocks, block_sorter)

	local last_block_height = 0
	local block
	for i = 1, #blocks do
		block = blocks[i]

		if self.highest_x + block.w > self.w then
			self.highest_y = self.highest_y + last_block_height
			if self.highest_y > self.h then
				return false, "Could not pack all."
			end

			self.highest_x = 0
		end

		if self.highest_y + block.h > self.h then
			return false, "Could not pack all."
		else
			block.fit = {x = self.highest_x, y = self.highest_y}
			self.highest_x = self.highest_x + block.w
		end

		last_block_height = block.h
	end

	return true
end

return packer