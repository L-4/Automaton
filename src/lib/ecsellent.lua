-- You think you have a minimalist class implementation? Hold my coffee.
local function class()
	local _class = setmetatable({}, {
		__call = function(self, ...)
			return setmetatable({}, self):__init(...)
		end,
	})
	_class.__index = _class
	return _class
end

-- assert() is slow. This is less slow.
local function assertx(val, ...)
  if not val then error(table.concat({...})) end
end

-- Forward declaring classes and arrays
local Component = class()
local components = {}

local System = class()
local systems = {}
local callbacks = {}

local Entity = class()
local entities = {}

--  ____   ___   __  __  ____    ___   _   _  _____  _   _  _____
-- / ___| / _ \ |  \/  ||  _ \  / _ \ | \ | || ____|| \ | ||_   _|
--| |    | | | || |\/| || |_) || | | ||  \| ||  _|  |  \| |  | |
--| |___ | |_| || |  | ||  __/ | |_| || |\  || |___ | |\  |  | |
-- \____| \___/ |_|  |_||_|     \___/ |_| \_||_____||_| \_|  |_|


function Component:__init(name, constructor)
	assertx(not components[name], "Component '", name, "' exists.")

	self.systems = {}
	self.instantiate = constructor

	local system
	for i = 1, #systems do
		system = systems[i]

		for j = 1, #system.required do
			if system.required[j] == name then
				self.systems[#self.systems+1] = system
			end
		end
	end

	components[name] = self
end

--  ____  __   __ ____   _____  _____  __  __
-- / ___| \ \ / // ___| |_   _|| ____||  \/  |
-- \___ \  \ V / \___ \   | |  |  _|  | |\/| |
--  ___) |  | |   ___) |  | |  | |___ | |  | |
-- |____/   |_|  |____/   |_|  |_____||_|  |_|

-- This function is used as a backup for the entityThink and
-- systemThink methods. It is faster than checking for their existance each time
local anonymous = function() end

function System:__init(name, system_type, required, entity_think, system_think)
	callbacks[system_type] = callbacks[system_type] or {} -- TODO: remove this check
																					-- Hi it's me from the future, what did you mean by this?
	assertx(not callbacks[system_type][name], "System '", tostring(system_type), ":", tostring(name), "' exists.")
	-- Make sure that a callback with this name doesn't exist
	assertx(type(required) == "table",
		"List of required components must be table. (You used '", type(required), ":", tostring(required), "')")
	local component_name
	for i = 1, #required do -- For each requirement
		component_name = required[i]
		assertx(components[component_name], "Component '", tostring(component_name), "' does not exist.")
		-- Make sure that the required component exists
		local other_component = components[component_name]
		if other_component then
			other_component.systems[#other_component.systems+1] = self
		end
	end

	self.required = required
	self.pool = {}
	self.data = {}

	self.entity_think = entity_think or anonymous
	self.system_think = system_think or anonymous

	local entity, found_all_components
	for i = 1, #entities do -- For each entity
		found_all_components = true
		entity = entities[i]

		for j = 1, #required do -- Get each required component
			if not entity:get(required[j]) then -- If the component was not found
				found_all_components = false break -- Break the loop
			end
		end

		if found_all_components then -- If the loop was never broken
			self.pool[#self.pool+1] = entity -- Add the entity to the system's pool
		end
	end

	systems[#systems+1] = self -- Add self to the list of systems
	callbacks[system_type][name] = self -- Register self in the list of callbacks

	return self
end

function System:does_require(component)
	for i = 1, self.requred do
		if component == self.requred[i] then
			return true
		end
	end
	return false
end

function System:add_entity(entity)
	self.pool[#self.pool+1] = entity
end

function System:remove_entity(entity)
	for key, other_entity in pairs(self.pool) do
		if other_entity == entity then
			self.pool[key] = nil
		end
	end
end

--  _____  _   _  _____  ___  _____ __   __
-- | ____|| \ | ||_   _||_ _||_   _|\ \ / /
-- |  _|  |  \| |  | |   | |   | |   \ V /
-- | |___ | |\  |  | |   | |   | |    | |
-- |_____||_| \_|  |_|  |___|  |_|    |_|

function Entity:__init()
	self.components = {}
	self.alive = true

	for i = 1, #entities do
		if not entities[i].alive then
			entities[i] = self
			return self
		end
	end
	entities[#entities+1] =self
	return self
end

function Entity:get(name)
	return self.components[name]
end

function Entity:is_in_pool(pool)
	for i = 1, #pool do
		if self == pool[i] then
			return true
		end
	end
	return false
end

function Entity:add(name, ...)
	-- Find all systems with component, add entity to system if
	-- entity components and required system components match
	assertx(components[name], "Component '" .. name .. "' does not exist.")
	assertx(not self.components[name], "Entity already has component '" .. name .. "'.")

	local component = components[name]

	self.components[name] = component:instantiate(self, ...)

	local system, found_all_components
	for i = 1, #component.systems do
		system = component.systems[i]
		if self:is_in_pool(system) then break end
		found_all_components = true
		for j = 1, #system.required do
			if not self:get(system.required[j]) then
				found_all_components = false break
			end
		end

		if found_all_components then
			system.pool[#system.pool+1] = self
		end
	end

	return self
end

function Entity:remove(name)
	-- Find all systems with components that math that of
	-- the entity, and remove the entity from the pool
	assertx(components[name], "Component '", name, "' does not exist.")
	assertx(self.components[name], "Entity does not have component '", name, "'.")

	local component = components[name]
	local system
	for i = 1, #component.systems do
		system = component.systems[i]
		system:remove_entity(self)
	end

	self.components[name] = nil
end

--   ___   _____  _   _  _____  ____
--  / _ \ |_   _|| | | || ____||  _ \
-- | | | |  | |  | |_| ||  _|  | |_) |
-- | |_| |  | |  |  _  || |___ |  _ <
--  \___/   |_|  |_| |_||_____||_| \_\

-- You put this in your love.run when inputs happen as follows:

-- if love.event then
-- 	love.event.pump()
-- 	for name, a,b,c,d,e,f in love.event.poll() do
-- 		if name == "quit" then
-- 			if not love.quit or not love.quit() then
-- 				return a or 0
-- 			end
-- 		end
-- 		love.handlers[name](a,b,c,d,e,f)
-- 		event(name,a,b,c,d,e,f)
-- 	end
-- end

-- and

-- event("draw")
-- event("update", dt)

-- At the same time that love.draw and love.update are called

-- Oh and this also means that you can create your own events and call them from anywhere in the code.

local function event(type, ...)
	if not callbacks[type] then return end
	for _, system in pairs(callbacks[type]) do
		system:system_think(...)

		local entity
		for j = 1, #system.pool do
			entity = system.pool[j]
			if entity.alive then
				system:entity_think(entity, ...)
			end
		end
	end
end

local function load_from_directory(directory)
	local files = love.filesystem.getDirectoryItems(directory)

	for i = 1, #files do
		local full_path = directory .. "/" .. files[i]
		if love.filesystem.getInfo(full_path, "file") then
			require(directory .. "." .. files[i]:match("(.+)%.lua"))
		end
	end
end

-- Return

return {
	-- Classes
	Component = Component,
	System = System,
	Entity = Entity,
	-- Other
	event = event,
	load_from_directory = load_from_directory
}