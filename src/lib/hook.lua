local Hook = {}

local hooks = {}

for event in pairs(love.handlers) do
	hooks[event] = {}
end

function Hook.new(event, hook)
	assert(type(hook) == "function", "Hooks must be functions")
	hooks[event][#hooks[event]+1]	= hook
	return #hooks[event]
end

function Hook.remove(event, hook_id)
	hooks[event][hook_id] = hooks[event][#hooks[event]]
	hooks[event][#hooks[event]] = nil
end

function Hook.get(event, hook_id)
	return hooks[event][hook_id]
end

function Hook.call(event, ...)
	local hook_list = hooks[event]
	for i = 1, #hooks do
		hook_list[i](...)
	end
end

return Hook