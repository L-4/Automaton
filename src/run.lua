local event = require "lib.ecsellent".event
local Camera = require "Automaton.Camera"
local call = require "lib.hook".call

local transform = love.math.newTransform():scale(1, -1)

return function()
	if love.load then love.load(love.arg.parseGameArguments(arg), arg) end

	-- We don't want the first frame's dt to include time taken by love.load.
	if love.timer then love.timer.step() end

	local dt = 0

	-- Main loop time.
	return function()
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a or 0
					end
				end
				love.handlers[name](a,b,c,d,e,f)
				event(name,a,b,c,d,e,f)
				call(name,a,b,c,d,e,f)
			end
		end

		-- Update dt, as we'll be passing it to update
		if love.timer then dt = love.timer.step() end

		-- Call update and draw
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
		event("update", dt)
		call("update", dt)

		if love.graphics and love.graphics.isActive() then
			love.graphics.replaceTransform(transform)
			local width, height = love.graphics.getDimensions()
			love.graphics.translate(-Camera.x + width / 2, -Camera.y - height / 2)
			love.graphics.clear(love.graphics.getBackgroundColor())
			if love.draw then love.draw() end
			event("draw")
			call("draw")
			love.graphics.origin()
			if love.ui then love.ui() end

			love.graphics.present()
		end

		if love.timer then love.timer.sleep(0.001) end
	end
end