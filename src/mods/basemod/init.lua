-- luacheck: globals Mod Player Camera

Mod.name = "basemod"

function Mod.register_tiles(register)

	local tiles_a = love.graphics.newImage(Mod.path .. "/tiles_a.png")
	local tiles_n = love.graphics.newImage(Mod.path .. "/tiles_n.png")

	register("bricks", 2, 0, 0, 32, 32, tiles_a)
	register("bricks_vine", 3, 32, 0, 32, 32, tiles_a)
	register("dirt", 4, 0, 32, 32, 32, tiles_a)
	register("cone", 5, 32, 32, 32, 32, tiles_a, tiles_n)

	return true
end

function Mod.initialize()
	function Player:update(dt)
		local dx, dy = 0, 0
		if love.keyboard.isDown("right") then
			dx = 500 * dt
		elseif love.keyboard.isDown("left") then
			dx = -500 * dt
		end

		if love.keyboard.isDown("up") then
			dy = 500 * dt
		elseif love.keyboard.isDown("down") then
			dy = -500 * dt
		end

		self:move(dx, dy)
	end

	Camera.follow(Player)

	return true
end