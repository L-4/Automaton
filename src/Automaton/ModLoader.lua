--- Loads, sandboxes, and runs mods
-- @module ModLoader

local Tile = require "Automaton.Tile"

local ModLoader = {} --- @todo Sort out the sandbox

local environment = {
	-- LOVE
	love = love,

	-- Lua
	error = error,
	print = print,

	-- Automaton

	Player = require "Automaton.Player",
	Camera = require "Automaton.Camera"
}

ModLoader.mods = {}

--- Loads the data from a mod into memory to be run later
-- @param path Path to load mod from
function ModLoader.preload(path)
	local file_type = love.filesystem.getInfo(path).type

	--- @todo Allow loading from zips. Once done, remove the ignore statement
	if file_type == "file" then --luacheck: ignore
		-- if path:match(".+%.(.+)") == "zip" then

		-- end
	elseif file_type == "directory" then
		if love.filesystem.getInfo(path .. "/init.lua") then
			local mod_loader, error_message = love.filesystem.load(path .. "/init.lua")

			if error_message then
				error("Error loading mod '" .. path .. "': " .. error_message)
			else
				environment.Mod = {path = path}

				setfenv(mod_loader, environment)()

				ModLoader.mods[environment.Mod.name] = environment.Mod
			end
		end
	end
end

--- Preloads mods in the mods/ folder
-- @see ModLoader.preload
function ModLoader.preload_default()
	local files = love.filesystem.getDirectoryItems("mods")

	for i = 1, #files do
		ModLoader.preload("mods/" .. files[i])
	end
end

--- Loads all preloaded mods
function ModLoader.load()
	for name, mod in pairs(ModLoader.mods) do
		if mod.register_tiles then
			print("Loading tiles for '" .. name .. "'...")
			assert(mod.register_tiles(Tile.register))
		end
	end

	Tile.pack()

	-- for name, mod in pairs(ModLoader.mods) do
	-- 	if mod.register_biomes then
	-- 		print("Loading biomes for '" .. name "'...")
	-- 		assert(mod.register_biomes(Biome.register))
	-- 	end
	-- end

	for name, mod in pairs(ModLoader.mods) do
		if mod.initialize then
			print("Initializing '" .. name .. "'...")
			assert(mod.initialize())
		end
	end
end

return ModLoader