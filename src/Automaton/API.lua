--- This module wraps the Automaton function and puts them in an environment for mods.
--- For now this is only for reference
-- @module API

local API = {}

API.Hook = {}

local Hook = require "lib.hook"

API.Hook.new = Hook.new
API.Hook.remove = Hook.remove
API.Hook.get = Hook.get

API.Camera = {}

local Camera = require "Automaton.Camera"

API.Camera.follow = Camera.follow
API.CAmera.screen_to_world = Camera.screen_to_world

API.Tile = {}

function API.Tile.register() end
function API.Tile.get() end

API.ECS = {}

local ECS = require "lib.ecsellent"

function API.ECS.component() end
function API.ECS.system() end
function API.ECS.entity() end

API.Light = {}

API.World = {}

function API.World.break_tile() end
function API.World.get_tile() end
function API.World.raycast() end

API.Mod = {}

function API.Mod.get_shared_table() end
function API.Mod.get_mod_table() end
function API.Mod.set_meta() end

return API