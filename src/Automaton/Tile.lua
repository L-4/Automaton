--- Handles textures that are registered from mods, and renders them to the tilemap
-- @module Tile

--- @todo This module could be renamed to something better like TilePacker

local packer = require "lib.packer"

local Tile = {}
local texture_size = 512

local Packer = packer(texture_size, texture_size)
Tile.albedo = love.graphics.newCanvas(texture_size, texture_size)
Tile.albedo:setFilter("nearest", "nearest")
Tile.normal = love.graphics.newCanvas(texture_size, texture_size)
Tile.normal:setFilter("nearest", "nearest")

Tile.tiles = { { name = "air", uv = { 0, 0, 0, 0 } } }
Tile.enums = { AIR = 1 }

local registered_tiles = {}

--- Registers a new tile for packing
-- @param name Name of tile. Will be converted to SCREAMING_SNAKE_CASE
-- @param id ID for tile. Must be unique
-- @param x x texture coordinate
-- @param y y texture coordinate
-- @param w texture width
-- @param h texture height
-- @param albedo Texture for albedo
-- @param normal Texture for normal map. NYI
function Tile.register(name, id, x, y, w, h, albedo, normal)
	assert(name and id and x and y and w and h and albedo, "You must supply name, id, texture coordinates and albedo")
	registered_tiles[#registered_tiles+1] = {name = name, id = id, albedo = albedo,
	normal = normal, x = x, y = y, w = w, h = h}
end

--- Packs tiles into the tilemap using First Fit
function Tile.pack()
	assert(Packer:pack(registered_tiles))

	local tile
	-- Set name and UVs
	for i = 1, #registered_tiles do
		tile = registered_tiles[i]
		Tile.enums[tile.name:upper():gsub(" ", "_")] = tile.id

		Tile.tiles[tile.id] = {
			name = tile.name,
			uv = {
				tile.fit.x / texture_size,
				tile.fit.y / texture_size,
				tile.fit.x / texture_size + tile.w / texture_size,
				tile.fit.y / texture_size + tile.h / texture_size
			}
		}
	end

	love.graphics.setCanvas(Tile.albedo)

	for i = 1, #registered_tiles do
		tile = registered_tiles[i]

		tile.albedo:setFilter("nearest", "nearest")

		local texture_w, texture_h = tile.albedo:getDimensions()

		love.graphics.draw(tile.albedo, love.graphics.newQuad(tile.x, tile.y, tile.w, tile.h,
			texture_w, texture_h), tile.fit.x, tile.fit.y)
	end

	love.graphics.setCanvas(Tile.normal)

	for i = 1, #registered_tiles do
		tile = registered_tiles[i]

		if tile.normal then
			tile.normal:setFilter("nearest", "nearest")

			local texture_w, texture_h = tile.normal:getDimensions()

			love.graphics.draw(tile.normal, love.graphics.newQuad(tile.x, tile.y, tile.w, tile.h,
				texture_w, texture_h), tile.fit.x, tile.fit.y)
		else -- @todo: Automatic normal map generation, or just write #7f7fff to the whole texture
			love.graphics.setColor(0x7f / 255, 0x7f / 255, 0xff)
			love.graphics.rectangle("fill", tile.fit.x, tile.fit.y, tile.w, tile.h)
			love.graphics.setColor(1, 1, 1)
		end
	end

	love.graphics.setCanvas()
end

return Tile