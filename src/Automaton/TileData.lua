--- Contains the basic data to describe a Tile
-- @module TileData

local ffi = require "ffi"

--- The TileData struct
-- @field ID Block ID
-- @field r Red component of color
-- @field g Green component of color
-- @field b Blue component of color
-- @table TileData
ffi.cdef[[
typedef struct {
	unsigned int ID;
	float r, g, b;
} Tile;
]]

local tile_data_metatable = {}
tile_data_metatable.__index = tile_data_metatable

local TileData = ffi.metatype("Tile", tile_data_metatable)

return TileData