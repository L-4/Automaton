--- A basic player class, will probably be removed in favor of an ECS solution
-- @module Player

--- @todo Remove player class.

local Player = require "lib/class"()

function Player:__init()
	self.x, self.y = 0, 0

	return self
end

function Player:move(delta_x, delta_y)
	self.x, self.y = self.x + delta_x, self.y + delta_y
end

function Player:update(dt) end  -- luacheck: ignore
function Player:draw() end      -- luacheck: ignore
function Player:tick(dt) end    -- luacheck: ignore

return Player()