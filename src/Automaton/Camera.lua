--- Entity that loads and renders visible chunks
-- @module Camera

local Chunk = require "Automaton.Chunk"
local Meta  = require "Automaton.Meta"
local mathx = require "lib.mathx"

local Camera = {x = 0, y = 0, smoothing = 2}

--- Tells camera to follow any reference to a table with x and y fields
-- @param entity Entity with x and y fields
function Camera.follow(entity)
	if not entity then Camera.following = nil return end
	if not entity.x and entity.y then error("Tried to follow entity without position") end
	Camera.following = entity
end

local last_chunk_check = 1

--- Reads a chunk from the chunk queue and unloads it if it is not on screen
function Camera.step_unload()
	if last_chunk_check > #Chunk.chunk_list then last_chunk_check = 1 end

	local chunk = Chunk.chunk_list[last_chunk_check]

	if not chunk then return end

	if chunk.loaded then
		local chunk_x, chunk_y = Meta.chunk_to_world(chunk.x, chunk.y)
		local width, height = love.graphics.getDimensions()
		if not mathx.aabb(chunk_x, chunk_y, Meta.chunk_absolute_size, Meta.chunk_absolute_size,
											Camera.x - width / 2, Camera.y - height / 2, width, height) then
			chunk:unload()
		end
	end

	last_chunk_check = last_chunk_check + 1
end

local load_interval = 0.1
local last_update = 0

--- Loads all chunks on the screen
function Camera.load_visible()
	--- @todo Make camera load chunks in direction it is moving, to prevent black bars.
	local width, height = love.graphics.getDimensions()
	local x1, y1 = Meta.world_to_chunk(Camera.x - width / 2, Camera.y - height / 2)
	local x2, y2 = Meta.world_to_chunk(Camera.x + width / 2, Camera.y + height / 2)

	for x = x1, x2 do
		for y = y1, y2 do
			Chunk(x, y)
		end
	end
end

--- Updates the state of the camera
-- @param dt Time delta since last update
function Camera.update(dt)
	if Camera.following then
		Camera.x, Camera.y = mathx.lerp_vec(Camera.x, Camera.y, Camera.following.x, Camera.following.y, Camera.smoothing * dt)
	end
	last_update = last_update + dt

	if last_update >= load_interval then

		Camera.load_visible()

		last_update = 0
	end
	Camera.step_unload()
end

function Camera.screen_to_world(screen_x, screen_y)
	local width, height = love.graphics.getDimensions()
	return Camera.x + screen_x - width / 2, Camera.y - screen_y + height / 2
end

return Camera