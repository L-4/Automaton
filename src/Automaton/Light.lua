local Light = require "lib.class"()

local Chunk = require "Automaton.Chunk"

local max_lights = 16 -- @todo synchronize this with shader.
local active_lights = 0

local lights = {}

function Light:__init(x, y, radius, r, g, b)
	if active_lights >= max_lights then
		error("too many lights") -- @todo Handle this error
	else
		active_lights = active_lights + 1
		self.id = active_lights

		self.position, self.color = {}, {}
		self:set_position(x, y)
		self:set_radius(radius)
		self:set_color(r, g, b)

		lights[self.id] = self
		Chunk.shader:send("active_lights", active_lights)
		return self
	end
end

function Light:set_position(x, y)
	self.position[1], self.position[2] = x, y
	Chunk.shader:send(("lights[%i].position"):format(self.id - 1), self.position)
end

function Light:set_radius(radius)
	self.radius = radius
	Chunk.shader:send(("lights[%i].radius"):format(self.id - 1), self.radius)
end

function Light:set_color(r, g, b)
	self.color[1], self.color[2], self.color[3] = r, g, b
	Chunk.shader:send(("lights[%i].color"):format(self.id - 1), self.color)
end

function Light:get_position()
	return self.position[1], self.position[2]
end

function Light:get_radius()
	return self.radius
end

function Light:get_color()
	return self.color[1], self.color[2], self.color[3]
end

function Light:destroy()
	lights[#lights] = lights[self.id]
	active_lights = active_lights - 1
	Chunk.shader:send("active_lights", active_lights)
end

return Light