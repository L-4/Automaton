--- Contains the pure data that defines a chunk
-- @module ChunkData

local ffi      = require "ffi"
local Meta     = require "Automaton.Meta"
local TileData = require "Automaton.TileData"

--- The ChunkData struct
-- @field tiles 2d array of TileDatas
-- @field x x coordinate of chunk
-- @field y y coordinate of chunk
-- @table ChunkData
ffi.cdef(string.format([[
typedef struct {
	Tile tiles[%i][%i];
	int x, y;
} ChunkData;
]], Meta.chunk_size, Meta.chunk_size))

local chunk_data_metatable = {}
chunk_data_metatable.__index = chunk_data_metatable
local ChunkData -- Forward declared for constructor

--- Generates a chunk using a generator function
-- @param chunk_x x coordinate of chunk
-- @param chunk_y y coordinate of chunk
-- @param generator_function function run for each tile, determining what tile to use.
-- Should return ID, flags, and rgb
-- @return chunk_data The basic data describing the chunk, used by the renderer etc
function chunk_data_metatable.generate(chunk_x, chunk_y, generator_function)
	local chunk_data = ChunkData() -- Create empty ChunkData
	chunk_data.x, chunk_data.y = chunk_x, chunk_y -- Set chunk coordinates
	for x = 0, Meta.chunk_size - 1 do
		for y = 0, Meta.chunk_size - 1 do -- Iterate through offsets from chunk corner
			-- Generator functions gets passed tile x, tile y
			-- Generator function should return ID, r, g, b
			chunk_data.tiles[x][y] = TileData(generator_function(
				x + chunk_x * Meta.chunk_size, -- Generate new TileData and put in ChunkData tile array
				y + chunk_y * Meta.chunk_size
			))
		end
	end

	return chunk_data
end

ChunkData = ffi.metatype("ChunkData", chunk_data_metatable)

return ChunkData