--- Contains all data pertaining to a chunk (ChunkData, chunk metadata...)
-- @module Chunk

local Meta        = require "Automaton.Meta"
local class       = require "lib.class"
local RenderChunk = require "Automaton.RenderChunk"
local ChunkData   = require "Automaton.ChunkData"
local Tile        = require "Automaton.Tile"

local Chunk = class()
--- @todo Use new class implementation
Chunk.chunk_list = {}
Chunk.chunks = {}
Chunk.shader = love.graphics.newShader("Automaton/shaders/world.frag")
if Chunk.shader:hasUniform("texture_normal") then
	Chunk.shader:send("texture_normal", Tile.normal)
end

function Chunk.generator_function(x, y)
	local height = math.floor(20 + love.math.noise(x / 15, 0) * 12)
	local id
	if y > height then
		id = Tile.enums.AIR
	elseif y == height then
		id = Tile.enums.DIRT
	else
		local stone_weight = love.math.noise(x / 10, y / 10)
		if stone_weight > 0.6 then
			id = Tile.enums.BRICKS_VINE
		else
			id = Tile.enums.BRICKS
		end
	end

	return id, 1, 1, 1
end

--- Initializer for chunk
-- @param chunk_x x coordinate of chunk
-- @param chunk_y y coordinate of chunk
function Chunk:__init(chunk_x, chunk_y)
	self.x, self.y = chunk_x, chunk_y
	self.data = ChunkData.generate(chunk_x, chunk_y, Chunk.generator_function)
	self.mesh = {}
	self:generate_mesh()
	-- self.physics = {}
	-- self:generate_physics()
	self.loaded = true

	if Chunk.chunks[chunk_x] and Chunk.chunks[chunk_x][chunk_y] then -- Chunk is in memory
		Chunk.chunks[chunk_x][chunk_y].loaded = true
		return Chunk.chunks[chunk_x][chunk_y]
	end

	if not Chunk.chunks[chunk_x] then Chunk.chunks[chunk_x] = {} end
	Chunk.chunks[chunk_x][chunk_y] = self

	for i = 1, #Chunk.chunk_list do
		if not Chunk.chunk_list[i].loaded then
			Chunk.chunk_list[i] = self
			return self
		end
	end

	Chunk.chunk_list[#Chunk.chunk_list+1] = self

	return self
end

--- Recreates mesh for entire chunk
function Chunk:generate_mesh()
	self.mesh = RenderChunk.generate(self.data)
end

--- Draws a chunk in world space
function Chunk:draw()
	if self.mesh then
		love.graphics.draw(self.mesh, self.x * Meta.chunk_absolute_size, self.y * Meta.chunk_absolute_size)
		-- love.graphics.print(tostring(self.x) .. ", " .. tostring(self.y), self.x * Meta.chunk_absolute_size, self.y * Meta.chunk_absolute_size, 0, 1, -1)
		-- love.graphics.rectangle("line", self.x * Meta.chunk_absolute_size, self.y * Meta.chunk_absolute_size, Meta.chunk_absolute_size, Meta.chunk_absolute_size)
	end
end

function Chunk.draw_all()
	love.graphics.setShader(Chunk.shader)
	for i = 1, #Chunk.chunk_list do
		if Chunk.chunk_list[i].loaded then
			Chunk.chunk_list[i]:draw()
		end
	end
	love.graphics.setShader()
end

--- Unloads a chunk
function Chunk:unload()
	self.loaded = false
	Chunk.chunks[self.x][self.y] = nil
end

--- Clears out all unloaded chunks from chunk buffer
function Chunk.collect_garbage() --- @todo Chunk garbage collector doesn't seem to be collecting everything
	local new_list = {}						 --- @todo Chunk garbage collector seems to collect chunks that are on the screen
	for i = 1, #Chunk.chunk_list do
		if Chunk.chunk_list[i].loaded then
			new_list[#new_list+1] = Chunk.chunk_list[i]
		end
	end
	Chunk.chunk_list = new_list
end

--- Helper function to get chunk. Does not assume that chunk exists
-- @param chunk_x x coordinate of chunk
-- @param chunk_y y coordinate of chunk
-- @return Chunk
function Chunk.get(chunk_x, chunk_y)
	assert(Chunk.chunks[chunk_x] and Chunk.chunks[chunk_x][chunk_y], "Could not find chunk " .. chunk_x .. ", " .. chunk_y)
	return Chunk.chunks[chunk_x][chunk_y]
end

--- Sets the tile selected with a world coordinate
-- @param world_x world x position
-- @param world_y world y position
-- @param tile TileData to replace with
function Chunk.set_tile(world_x, world_y, tile)
	local chunk_x, chunk_y = Meta.world_to_chunk(world_x, world_y)
	local tile_x, tile_y = Meta.chunk_tile_offset(world_x, world_y)
	print("tile offset", tile_x, tile_y)

	local chunk = Chunk.get(chunk_x, chunk_y)

	chunk.data.tiles[tile_x][tile_y] = tile
	chunk:generate_mesh()
end

return Chunk