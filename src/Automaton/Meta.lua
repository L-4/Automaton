--- Contains metadata and helper functions pertaining to metadata
-- @module Meta

local Meta = {
	chunk_size = 16,
	tile_size = 32
}

Meta.chunk_absolute_size = Meta.chunk_size * Meta.tile_size

--- Converts world space to chunk space
-- @param world_x x coordinate in world space
-- @param world_y y coordinate in world space
-- @return x coordinate in chunk space
-- @return y coordinate in chunk space
function Meta.world_to_chunk(world_x, world_y)
	return math.floor(world_x / Meta.chunk_absolute_size), math.floor(world_y / Meta.chunk_absolute_size)
end

--- Converts chunk space to world space (bottom right corner)
-- @param chunk_x x coordinate in chunk space
-- @param chunk_y y coordinate in chunk space
-- @return x coordinate in world space
-- @return y coordinate in world space
function Meta.chunk_to_world(chunk_x, chunk_y)
	return chunk_x * Meta.chunk_absolute_size, chunk_y * Meta.chunk_absolute_size
end

local floor = math.floor

--- Returns the relative tile from a world
-- @param world_x world x coordinate
-- @param world_y world y coordinate
-- @return x relative tile to chunk
-- @return y relative tile to chunk
function Meta.chunk_tile_offset(world_x, world_y)
	local relative_x, relative_y = world_x % Meta.chunk_absolute_size, world_y % Meta.chunk_absolute_size

	return floor(relative_x / Meta.tile_size), floor(relative_y / Meta.tile_size)
end

return Meta