#define MAX_LIGHTS 16

uniform sampler2D texture_normal;
uniform int active_lights = 0;

uniform bool mode = false;

const float light_height = 15.0;

uniform struct Light
{
	vec2 position;
	float radius;
	vec3 color;
} lights[MAX_LIGHTS];

vec4 effect(vec4 color, sampler2D texture_albedo, vec2 texture_coords, vec2 screen_coords)
{
	vec4 albedo_sample = Texel(texture_albedo, texture_coords);
	vec4 normal_sample = Texel(texture_normal, texture_coords) * 2 - 1;
	vec3 light_affect = vec3(0.0);
	for (int i = 0; i < active_lights; i++)
	{
		vec3 delta = normalize(vec3(lights[i].position - screen_coords, light_height));
		float dist = distance(screen_coords, lights[i].position);
		float light_strength = clamp(1.0 - dist / lights[i].radius, 0.0, 1.0);
		float normal_affect = ((dot(normal_sample.rgb, delta) + 1) / 2);
		light_affect += lights[i].color * light_strength * normal_affect;
	}
	if (mode) {
		return vec4(vec3(clamp(light_affect, 0.0f, 1.0f)), 1);
	} else {
		return albedo_sample * vec4(clamp(light_affect, vec3(0.0f), vec3(1.0f)), 1);
	}
	//return albedo_sample * clamp(normal_affect, 0.0f, 1.0f) * vec4(clamp(light_affect, vec3(0.0f), vec3(1.0f)), 1);
	//return albedo_sample * clamp(normal_affect, 0.0f, 1.0f) * vec4(clamp(light_affect, vec3(0.0f), vec3(1.0f)), 1);
}