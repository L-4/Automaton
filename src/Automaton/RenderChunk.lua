--- Handles creation of renderable chunks
-- @module RenderChunk

local Meta  = require "Automaton.Meta"
local Tile  = require "Automaton.Tile"

local function generate_tile_vertices(x, y, r, g, b, u1, v1, u2, v2)
	x, y = x * Meta.tile_size, y * Meta.tile_size
	return { x, y, u1, v2, r, g, b},
				 { x + Meta.tile_size, y, u2, v2, r, g, b },
				 { x, y + Meta.tile_size, u1, v1, r, g, b },
				 { x+ Meta.tile_size, y, u2, v2, r, g, b },
				 { x, y + Meta.tile_size, u1, v1, r, g, b },
				 { x + Meta.tile_size, y + Meta.tile_size, u2, v1, r, g, b }
end

local RenderChunk = {}

--- Generates a mesh from a ChunkData
-- @see ChunkData
-- @param chunk ChunkData
-- @return Mesh
function RenderChunk.generate(chunk)
	local vertices = {} -- OPTIMIZATION: Can reuse this table
	local tile, tile_data
	for x = 0, Meta.chunk_size - 1 do
		for y = 0, Meta.chunk_size - 1 do
			tile = chunk.tiles[x][y]
			if tile.ID ~= Tile.enums.AIR then
				tile_data = Tile.tiles[tile.ID]
				vertices[#vertices+1],vertices[#vertices+2],vertices[#vertices+3],
				vertices[#vertices+4],vertices[#vertices+5],vertices[#vertices+6]
				= generate_tile_vertices(x, y,
					tile.r, tile.g, tile.b, tile_data.uv[1], tile_data.uv[2], tile_data.uv[3], tile_data.uv[4])
			end
		end
	end

	local mesh
	if #vertices > 0 then
		mesh = love.graphics.newMesh(vertices, "triangles", "static")
		mesh:setTexture(Tile.albedo)
	end
	return mesh
end

return RenderChunk